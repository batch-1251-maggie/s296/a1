let http = require("http");
const PORT = 3000;

http.createServer((req, res)=>{
	//uri or url/endpoint = resource
	//http method

	console.log(req);


	if(req.url === "/profile" && req.method === "GET"){
		res.writeHead( 200, {"Content-Type": "text/plain"});
		res.end("Welcome to my page");
	}else if(req.url === "/register" && req.method === "POST"){
		res.writeHead( 200, {"Content-Type": "text/plain"});
		res.end("Data to be sent to the database");
	}else{
		res.writeHead( 404, {"Content-Type": "text/plain"});
		res.end("Request cannot be completed");
	}




}).listen(PORT);

console.log(`Server is now connected to port ${PORT}`);